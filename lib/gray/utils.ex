defmodule Gray.Utils do
  @moduledoc """
  Contains utility methods for this package
  """
  
  def to_binary(0), do: 0
  def to_binary(num) when is_integer(num) and num >= 0 do
    to_binary(num, [])
  end

  defp to_binary(0, list), do: Enum.reduce(list, "", fn (ele, acc) -> acc <> to_string(ele) end)
  defp to_binary(num, list), do: to_binary(div(num, 2), [rem(num, 2) | list])
end