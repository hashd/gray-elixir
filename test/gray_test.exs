defmodule GrayTest do
  use ExUnit.Case
  import Gray, only: [binary_to_gray: 1, gray_to_binary: 1]

  test "Binary to gray and then back should yield same result" do
    for i <- 0..10000000 do
      assert (i |> binary_to_gray |> gray_to_binary) == i
    end
  end
end
